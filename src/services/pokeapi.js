export default (ngModule) => {

    ngModule.factory('pokeapi', [
        '$http',
        function ($http) {
            const pokeapiUrl = 'http://pokeapi.co/api/v2/';

            return {
                getPokemons,
                getPokemon,
                updatePokemon
            };

            function getPokemons(page) {
                return $http.get(pokeapiUrl + 'pokemon/?limit=60&offset=' + (page * 60))
                    .then(getPokemonsComplete)
                    .catch(getPokemonsFailed);

                function getPokemonsComplete(response) {
                    return response.data;
                }

                function getPokemonsFailed(error) {
                    return {error}
                }
            }

            function getPokemon(name) {
                return $http.get(pokeapiUrl + 'pokemon/' + name)
                    .then(getPokemonComplete)
                    .catch(getPokemonFailed);

                function getPokemonComplete(response) {
                    return response.data;
                }

                function getPokemonFailed(error) {
                    return {error}
                }
            }

            function updatePokemon(pokemon) {
                return $http.post(pokeapiUrl + 'pokemon/update' + name)
                    .then(getPokemonComplete)
                    .catch(getPokemonFailed);

                function getPokemonComplete(response) {
                    return response.data;
                }

                function getPokemonFailed(error) {
                    return {error}
                }
            }


        }]);
}