const angular = require('angular');
require('angular-ui-bootstrap');
require('angular-route');
require('angular-sanitize');


const ngModule = angular
    .module('app', ['ui.bootstrap', 'ngRoute', 'ngSanitize', 'toastr'])
    .config(['$routeProvider', function ($routeProvider) {

        // Specify the three simple routes ('/', '/About', and '/Contact')
        $routeProvider.when('/', {
            template: require('./views/main.html'),
            controller: 'HomeCtrl',
            controllerAs: 'hc'
        });
        $routeProvider.when('/pokemons/:page', {
            template: require('./views/main.html'),
            controller: 'HomeCtrl',
            controllerAs: 'hc'
        });
        $routeProvider.when('/pokemon/:name', {
            template: require('./views/pokemon.html'),
            controller: 'PokemonCtrl',
            controllerAs: 'pc'
        });
        $routeProvider.otherwise({
            redirectTo: '/'
        });
        }]
    );

require('./controllers')(ngModule);
require('./directives')(ngModule);
require('./services')(ngModule);

require('./index.scss');
