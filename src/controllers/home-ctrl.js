export default (ngModule) => {
    ngModule.controller('HomeCtrl', ['$scope', 'pokeapi', '$routeParams', function ($scope, pokeapi, $routeParams) {

        let vm = this;
        vm.loading = false;
        vm.currentPage = $routeParams.page || 1;

        vm.getPokemon = (page) => {
            vm.loading = true;
            return pokeapi.getPokemons(page)
                .then(function(data) {
                    vm.loading = false;
                    vm.pokemons = data.results;
                    console.log(vm.pokemons);
                    let pagesCount = Math.ceil(data.count / 60);
                    vm.pages = [];
                    for (let i = 0; i < pagesCount; i++){
                        vm.pages.push({
                            number: (i+1),
                            url: '/#!/pokemons/' + ((i+1)).toString()
                        });
                    }
                    console.log(vm.pages);
                });
        };

        vm.getPokemon(vm.currentPage - 1);

    }]);
}