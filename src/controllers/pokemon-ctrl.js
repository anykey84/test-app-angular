export default (ngModule) => {
    ngModule.controller('PokemonCtrl', ['$scope', 'pokeapi', '$routeParams', '$window', 'toastr', function ($scope, pokeapi, $routeParams, $window, toastr) {

        let vm = this;
        vm.loading = false;
        vm.pokemonName = $routeParams.name;
        if (!vm.pokemonName) $window.location = '/';

        vm.getPokemon = (name) => {
            vm.loading = true;
            return pokeapi.getPokemon(name)
                .then(function(data) {
                    vm.loading = false;
                    vm.pokemonData = data;
                    vm.pokemon = angular.copy(data);
                    console.log(vm.pokemon);

                });
        };

        vm.go_back = function() {
            $window.history.back();
        };

        vm.getPokemon(vm.pokemonName);

        vm.cancelEdit = () => {
            vm.editMode = false;
            vm.pokemon = angular.copy(vm.pokemonData);
        };

        vm.updatePokemon = () => {
            vm.editMode = false;
            vm.loading = true;

            return pokeapi.updatePokemon(vm.pokemon)
                .then(function(data) {
                    vm.loading = false;
                    console.log(data);
                    if (data.error){
                        vm.editMode = true;
                        //vm.pokemon = angular.copy(vm.pokemonData);
                        toastr.error(data.error.statusText, 'Ошибка сохранения');
                    } else {
                        vm.pokemonData = data;
                        vm.pokemon = angular.copy(data);
                        toastr.success('успешное сохранение', '');
                    }
                });
        };


    }]);
}