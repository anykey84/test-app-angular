export default (ngModule) => {
    require('./home-ctrl.js')(ngModule);
    require('./pokemon-ctrl.js')(ngModule);
};